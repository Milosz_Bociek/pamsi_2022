#include <string>
class Element{
  std::string name;
  std::string index;  
public:

    Element() = delete;
    Element(std::string _name, std::string _index);
    void setName(std::string _name);
    void setIndex(std::string _index);

    std::string getName();
    std::string getIndex();

};