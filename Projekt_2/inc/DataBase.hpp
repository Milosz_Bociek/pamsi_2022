#include <vector>
#include <Element.hpp>
class DataBase : Element {

std::vector<Element*> data;
uint32_t size;

public:
    DataBase();
    bool ReadFormFile();
    uint32_t getSize();
    Element* getElementByIndex(uint32_t index);
    bool swapElements(Element* p, Element* q);

};