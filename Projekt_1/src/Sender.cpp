#include "Sender.hpp"

Sender::Sender(){this->WektorSize = 0;}

void Sender::AddElement(std::string element){
    std::pair<std::string, int> temp;
    temp.first = element;
    temp.second = this->WektorSize;
    this->Wektor.push_back(temp);
    this->WektorSize++;
}

void Sender::AddElementAndKey(std::string element, int key){
    std::pair<std::string, int> temp;
    temp.first = element;
    temp.second = key;
    this->Wektor.push_back(temp);
    this->WektorSize++;
}

void Sender::DeleteLastElement(){
    if(this->WektorSize > 0){
        this->Wektor.pop_back();
        this->WektorSize--;
    }
}

void Sender::Display(){
    for(int i = 0; i < this->WektorSize; i++){
        std::cout<<"|  "<<(this->Wektor[i].first)<< "  ";
    }
    std::cout<<"|"<<std::endl;
}

void Sender::DisplayKey(){
    for(int i = 0; i < this->WektorSize; i++){
        std::cout<<"|  "<<this->Wektor[i].second<<"  ";
    }
    std::cout<<"|"<<std::endl;
}

void Sender::ShuffleElements(){
    unsigned num = std::chrono::system_clock::now().time_since_epoch().count();
    std::shuffle(Wektor.begin(), Wektor.end(), std::default_random_engine(num));    
}

std::string Sender::GetLastElementValue(){
    return this->Wektor[this->WektorSize-1].first; 
}

std::pair<std::string, int> Sender::GetLastElement(){
    //if(!this->Wektor.empty()){
        std::pair<std::string, int> tmp = this->Wektor[this->WektorSize-1];     
        this->Wektor.pop_back();
        this->WektorSize--;
        return tmp;
    //}
}

std::string Sender::GetLastElementByValue(){
    //if(!this->Wektor.empty()){
        std::pair<std::string, int> tmp = this->Wektor[this->WektorSize-1];     
        this->Wektor.pop_back();
        this->WektorSize--;
        return tmp.first;
    //}
}

bool Sender::ReadDataFromFileByWords(std::string fileName){
    std::string ReadBuffer;
    FileInput.open(fileName);
    if(!FileInput){
        return false;
    }
    while(!FileInput.eof()){
        FileInput >> ReadBuffer;
        this->AddElement(ReadBuffer);
    }
    return true;
}

bool Sender::ReadDataFromFileByLines(std::string fileName){
    std::string ReadBuffer;
    int temp;
    this->FileInput.open(fileName);
    if(!this->FileInput){
        return false;
    }
    while(FileInput.good()){
        FileInput >> ReadBuffer;
        for (char const &c : ReadBuffer) {
            if (std::isdigit(c) == 0){
                std::cerr<<"Problem"<<std::endl;
                return false;
            } 
        }
        temp = stoi(ReadBuffer);
        //std::cout<<"TEMP: "<<temp<<std::endl;
        std::getline(this->FileInput, ReadBuffer, '\n');
        ReadBuffer += '\n';
        this->AddElementAndKey(ReadBuffer, temp);
        std::cout<<ReadBuffer<<std::endl;   
    }
    
    return true;
    // while(!FileInput.eof()){
    //     if(ReadBuffer.find_first_not_of("0123456789") != std::string::npos){
    //         return false;
    //     }
    //     this->AddElement(ReadBuffer);
    //     FileInput >> ReadBuffer;
    // }
    // return true;

    
}


int Sender::getSize(){
    return this->WektorSize;
}
