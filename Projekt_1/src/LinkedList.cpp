#include "LinkedList.hpp"


// template<typename E>
// void LinkedList<E>::AddFront(E elem){
//     ListNode<E>* temp = head;
//     this->head = new ListNode<E>;
//     head->setData(elem);
//     head->setNext(temp);
//     this->size++;
//     this->setKey(size);
//     //std::cout << "AddFront: " << elem << std::endl;
// }

// template<typename E>
// void LinkedList<E>::DeleteFront(){
//     if(!this->isEmpty()){
//         ListNode<E>* temp = this->head->getNext();
//         delete this->head;
//         this->head = temp;
//         this->size--;
//         //std::cout << "DeleteFront" << std::endl;
//     }
// }

template<typename E>
void LinkedList<E>::removeFront(){
    if(!this->isEmpty()){
        ListNode<E>* temp = this->head;
        this->head = this->head->getNext();
        this->head->getNext()->setPrevious(NULL);
        delete temp;
        this->size--;
    }
}

template<typename E>
void LinkedList<E>::removeTail(){
    if(!this->isEmpty()) {
        if(this->head->getNext() == NULL) {
            this->head = NULL;
        } else {
            ListNode<E>* temp = this->tail;
            this->tail = this->tail->getPrevious();
            this->tail->setNext(NULL);
            delete temp; 
        }
        this->size--;
    }
}

template<typename E>
void LinkedList<E>::removeNode(ListNode<E>* del){
    if(!isEmpty()) {
        if(del == this->head && del == this->tail){
            this->tail = NULL;
            this->head = NULL;
        }
        else if(del == this->head){
            this->head = del->getNext();
            this->head->setPrevious(NULL);
        }
        else if(del == this->tail){
            this->tail = del->getPrevious();
            this->tail->setNext(NULL);
        }
        else{
            del->getNext()->setPrevious(del->getPrevious());
            del->getPrevious()->setNext(del->getNext());
        }
        delete del;
        this->size--;
    }
}

template<typename E>
void LinkedList<E>::Display(){
    if(!this->isEmpty()){
        ListNode<E>* temp = this->head;
        std::cout << "|  ";
        while(temp->getNext() != NULL){
            std::cout << temp->getData() << "  |";
            temp = temp->getNext();
           
        }
        std::cout << temp->getData() << "  |"<< std::endl;
    }
    else{
        std::cout << "List is empty" << std::endl;
    }
}

template<typename E>
void LinkedList<E>::DisplayKey(){
    if(!this->isEmpty()){
        ListNode<E>* temp = this->head;
        std::cout << "|  ";
        while(temp->getNext() != NULL){
            std::cout << temp->getKey() << "  |  ";
            temp = temp->getNext();
            
        }
        std::cout << temp->getKey() << "  |"<< std::endl;
    }
    else{
        std::cout << "List is empty" << std::endl;
    }
}

template<typename E>
void LinkedList<E>::enQueue(std::pair<E, int> elem){

    ListNode<E>* newNode = new ListNode<E>;
    
    newNode->setData(elem.first);
    newNode->setKey(elem.second);

    newNode->setNext(NULL); 
    newNode->setPrevious(NULL);

    if(this->isEmpty()) {
        this->tail = newNode;
        this->head = newNode;
    } 
    else{
        this->tail->setNext(newNode);
        newNode->setPrevious(this->tail);
        this->tail = newNode;
    } 
    this->size++;
}

template<typename E>
E LinkedList<E>::deQueueMin(){
    if(!this->isEmpty()){
        int min;
        E returnValue;
        ListNode<E>* search = this->tail;
        ListNode<E>* temp;
        min = this->tail->getKey();
        while(search!= NULL){
            if(min >= search->getKey()){
                min = search->getKey();
                temp = search;
            }
            search = search->getPrevious();
        }

        returnValue = temp->getData();
        this->removeNode(temp);
        return returnValue;
    }
    return 0;
}

template<typename E>
E LinkedList<E>::deQueueMax(){
    if(!this->isEmpty()){
        int min;
        E returnValue;
        ListNode<E>* search = this->tail;
        ListNode<E>* temp;
        min = this->tail->getKey();
        while(search!= NULL){
            if(min <= search->getKey()){
                min = search->getKey();
                temp = search;
            }
            search = search->getPrevious();
        }

        returnValue = temp->getData();
        this->removeNode(temp);
        return returnValue;
    }
    return 0;
}

template<typename E>
bool LinkedList<E>::isEmpty(){
    if(this->size > 0){
        return false;
    } 
    return true;
}

template<typename E>
int LinkedList<E>::frontKey(){
    if(!this->isEmpty()){
        return this->head->getKey();
    }
    return -1;
}

template<typename E>
int LinkedList<E>::getSize(){
    return this->size;
}



template class LinkedList<int>;
template class LinkedList<char>;
template class LinkedList<std::string>;
