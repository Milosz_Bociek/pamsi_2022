#include "PriorityQueue.hpp"

template<typename E>
E PriorityQueue<E>::deQueueMin(){
    if(!this->isEmpty()){
        int min;
        E returnValue;
        ListNode<E>* search = this->head;
        ListNode<E>* temp;
        min = this->head->getKey();
        while(search!= NULL){
            if(min >= search->getKey()){
                min = search->getKey();
                temp = search;
            }
            search = search->getNext();
        }

        returnValue = temp->getData();
        this->removeMiddle(temp);
        return returnValue;
    }
    return 0;
}


template<typename E>
E PriorityQueue<E>::deQueueMax(){
    if(!this->isEmpty()){
        int min;
        E returnValue;
        ListNode<E>* search = this->head;
        ListNode<E>* temp;
        min = this->head->getKey();
        while(search!= NULL){
            if(min <= search->getKey()){
                min = search->getKey();
                temp = search;
            }
            search = search->getNext();
        }

        returnValue = temp->getData();
        this->removeMiddle(temp);
        return returnValue;
    }
    return 0;
}

template<typename E>
void PriorityQueue<E>::enQueue(std::pair<E, int> elem){

    ListNode<E>* newNode = new ListNode<E>;
    
    newNode->setData(elem.first);
    newNode->setKey(elem.second);

    newNode->setNext(NULL); 
    newNode->setPrevious(NULL);

    if(this->isEmpty()) {
        this->tail = newNode;
        this->head = newNode;
    } 
    else{
        this->tail->setNext(newNode);
        newNode->setPrevious(this->tail);
        this->tail = newNode;
    } 
    this->size++;
}



template class PriorityQueue<int>;
template class PriorityQueue<std::string>;
