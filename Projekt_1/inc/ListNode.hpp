#ifndef LISTNODE_HPP
#define LISTNODE_HPP

#include <stdlib.h>

template<typename E>
class ListNode{
    
    E data;
    int Key;
    ListNode<E>* next = NULL;
    ListNode<E>* previous = NULL;
    
public:
    E getData(){return this->data;};
    ListNode<E>* getNext(){return this->next;};
    ListNode<E>* getPrevious(){return this->previous;};
    int getKey(){return this->Key;};
    void setData(E nElem){this->data = nElem;};
    void setKey(int nKey){this->Key = nKey;};
    void setNext(ListNode<E>* nNext){this->next = nNext;};
    void setPrevious(ListNode<E>* nPrevious){this->previous = nPrevious;};
};


#endif