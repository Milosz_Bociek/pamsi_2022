#ifndef Sender_HPP
#define Sender_HPP

#include <chrono> 
#include <algorithm>
#include <vector> 
#include <random>
#include <iostream>
#include <string>
#include <fstream>


class Sender{
private:
std::ifstream FileInput;
std::vector<std::pair<std::string,int>> Wektor;
int WektorSize;
public:
Sender();
void AddElement(std::string ElementW);
void AddElementAndKey(std::string ElementW, int Key);
int getSize();
void DeleteLastElement();
void ShuffleElements();
void Display();
void DisplayKey();
bool ReadDataFromFileByWords(std::string fileName);
bool ReadDataFromFileByLines(std::string fileName);

std::string GetLastElementValue();
std::string GetLastElementByValue();
std::pair<std::string, int> GetLastElement();

};

#endif