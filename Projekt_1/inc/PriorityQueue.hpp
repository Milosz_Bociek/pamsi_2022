#ifndef PRIORITYQUEUE_HH
#define PRIORITYQUEUE_HH

#include "LinkedList.hpp"

template<typename E>
class PriorityQueue : LinkedList<E>{
    LinkedList<E> list;
public:
    void enQueue(std::pair<E, int> elem);
    E deQueueMax();
    E deQueueMin();


};

#endif