#ifndef LINKEDLIST_HPP
#define LINKEDLIST_HPP

#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>

#include "ListNode.hpp"

template <typename E>
class LinkedList : ListNode<E>{

    ListNode<E>* head = NULL;
    ListNode<E>* tail = NULL;
    int size = 0;

    void AddFront(E elem);
    void DeleteFront();
    void removeFront();
    void removeTail();

    void removeNode(ListNode<E>* del);

    bool isEmpty();
    int getSize();   
    

    int frontKey();

public:
    void enQueue(std::pair<E, int> elem);
    E deQueueMax();
    E deQueueMin();

    void Display();
    void DisplayKey();
    void DisplayRaw();
    
};
#endif
